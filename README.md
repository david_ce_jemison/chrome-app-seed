chrome-app-seed
===============

Seed project for Chrome packaged app

####Create

1. Clone this repo!
2. Put your app with all dependencies inside the app folder. Note: dependencies can go anywhere inside the project; the app folder is there for convenience.
3. Customize 'chrome-app.html'. This is your 'index.html' when packaged.
4. Modify manifest.json as needed. For example, change the name of your app.

#### Install

 * go to Tools|Extensions
 * enable Developer Mode 
 * click "Load Unpackaged Extension" and choose the app folder (e.g. "chrome-app-seed")
 
Now you can launch the application from that page or from the Chrome Apps menu!